﻿using System.Globalization;

namespace TollWorkstation.Shared
{
   public static class FormattingExtensions
   {
      public static string ToCurrency(this double amount)
      {
         return amount.ToString("C", new CultureInfo("en-US"));
      }

      public static string ToCurrency(this decimal amount)
      {
         return amount.ToString("C", new CultureInfo("en-US"));
      }
   }
}
