﻿using System.Collections.Generic;

namespace TollWorkstation.Shared
{
   public class AppState
   {
      public Account SelectedSearchAccount { get; set; }

      public long AccountId = 100000001;

      public List<Account> Accounts { get; set; } = new List<Account>();

      public AppState()
      {
         AddAccount(new Account
         {
            Name = "Michael Scinocca",
            PhoneNumber = "647-818-4310",
            AccountBalance = 100,
         });

         AddAccount(new Account
         {
            Name = "Rahul Mehra",
            PhoneNumber = "647-818-4310",
            AccountBalance = 200,
         });
      }

      public void AddAccount(Account account)
      {
         account.AccountId = AccountId.ToString();
         AccountId++;

         Accounts.Add(account);
      }
   }
}
