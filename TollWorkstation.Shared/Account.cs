﻿using System.ComponentModel.DataAnnotations;

namespace TollWorkstation.Shared
{
   public class Account
   {
      public string AccountId { get; set; }

      [Required]
      [StringLength(10, ErrorMessage = "Name is too long.")]
      public string Name { get; set; }

      public string DisplayName { get { return string.IsNullOrEmpty(Name) ? "New" : Name; } }

      [Required]
      public string PhoneNumber { get; set; }

      public double AccountBalance { get; set; }

      public override bool Equals(object obj)
      {
         if(!(obj is Account account))
         {
            return false;
         }

         return AccountId.Equals(account.AccountId);
      }

      public override int GetHashCode()
      {
         return AccountId.GetHashCode();
      }
   }
}